/*
  * PoTD Provider for Wiki Loves Monuments - Panamá
 * Copyright 2013  David E. Narváez <david@li177-19.members.linode.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef WLMPAPOTDPROVIDER_H
#define WLMPAPOTDPROVIDER_H

#include "potdprovider.h"

class KUrl;
class KJob;

namespace KIO
{
    class Job;
}

class WLMPAPotdProvider : public PotdProvider
{
    Q_OBJECT

public:
    WLMPAPotdProvider(QObject* parent, const QVariantList &args = QVariantList());
    virtual QImage image() const;
    virtual QString identifier() const;

private slots:
    void trasnferedURL(KJob* job);
    void transferedData(KIO::Job* job, QByteArray data);
    void transferFinished(KJob* job);

private:
    void fetchImageURL(QString imageTitle);
    void fetchImage(KUrl imageURL);

    QByteArray m_imageData;
};

#endif // WLMPAPOTDPROVIDER_H
