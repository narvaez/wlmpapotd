project(wlmpapotd)

find_package(KDE4 REQUIRED)

include (KDE4Defaults)
include(MacroLibrary)
include(MacroOptionalDependPackage)

include_directories (${KDE4_INCLUDES})

set(potd_wlmpapotd_provider_SRCS wlmpapotdprovider.cpp)

kde4_add_plugin(plasma_potd_wlmpapotdprovider ${potd_wlmpapotd_provider_SRCS})
target_link_libraries(plasma_potd_wlmpapotdprovider plasmapotdprovidercore ${KDE4_KIO_LIBS} ${KDE4_KDECORE_LIBS} ${QT_QTGUI_LIBRARY} ${QT_QTSQL_LIBRARY})
install(TARGETS plasma_potd_wlmpapotdprovider DESTINATION ${PLUGIN_INSTALL_DIR})
install(FILES wlmpa2012.db DESTINATION ${DATA_INSTALL_DIR}/wlmpapotd)
install(FILES wlmpapotdprovider.desktop DESTINATION ${SERVICES_INSTALL_DIR})
