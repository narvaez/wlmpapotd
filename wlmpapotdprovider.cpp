/*
 * PoTD Provider for Wiki Loves Monuments - Panamá
 * Copyright 2013  David E. Narváez <david@li177-19.members.linode.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "wlmpapotdprovider.h"

#include <QImage>
#include <QDomDocument>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>

#include <KIO/Job>
#include <KUrl>
#include <KStandardDirs>
#include <KDebug>

POTDPROVIDER_EXPORT_PLUGIN( WLMPAPotdProvider, "WLMPAPotdProvider", "" )

WLMPAPotdProvider::WLMPAPotdProvider(QObject* parent, const QVariantList& args)
: PotdProvider(parent, args)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(KGlobal::dirs()->findResource("data", "wlmpapotd/wlmpa2012.db"));

    if(db.open())
    {
        QSqlQuery query("SELECT title FROM images ORDER BY RANDOM() LIMIT 1", db);

        if(query.exec() && query.first())
        {
            fetchImageURL(query.record().value("title").toString());
        }
    }
}

QImage WLMPAPotdProvider::image() const
{
    return QImage::fromData(m_imageData);
}

QString WLMPAPotdProvider::identifier() const
{
    return QLatin1String("wlmpapotd");
}

void WLMPAPotdProvider::trasnferedURL(KJob* job)
{
    KIO::StoredTransferJob * apiJob = qobject_cast<KIO::StoredTransferJob *>(job);
    QDomDocument apiResponse;
    QString domError;
    int domErrorLine;
    int domErrorColumn;
    QDomNodeList imageInfoNodes;

    if(!apiResponse.setContent(apiJob->data(), false, &domError, &domErrorLine, &domErrorColumn))
    {
        qDebug() << domError;
        qDebug() << " at " << domErrorLine << ":" << domErrorColumn;
    }

    imageInfoNodes = apiResponse.elementsByTagName("ii");

    if(imageInfoNodes.count() > 0)
    {
        KUrl imageURL(imageInfoNodes.at(0).attributes().namedItem("url").nodeValue());

        fetchImage(imageURL);
    }
}

void WLMPAPotdProvider::transferedData(KIO::Job* job, QByteArray data)
{
    Q_UNUSED(job);
    m_imageData.append(data);
}

void WLMPAPotdProvider::transferFinished(KJob* job)
{
    Q_UNUSED(job);
    emit finished(this);
}

void WLMPAPotdProvider::fetchImageURL(QString imageTitle)
{
    QString apiCall = QString("http://commons.wikimedia.org/w/api.php?action=query&titles=%1&prop=imageinfo&iiprop=url&format=xml").arg(imageTitle);
    KIO::StoredTransferJob * apiJob = KIO::storedGet(KUrl(apiCall), KIO::NoReload, KIO::HideProgressInfo);

    connect(apiJob, SIGNAL(result(KJob*)), this, SLOT(trasnferedURL(KJob*)));
}

void WLMPAPotdProvider::fetchImage(KUrl imageURL)
{
    KIO::TransferJob * imageJob = KIO::get(imageURL, KIO::NoReload, KIO::HideProgressInfo);

    connect(imageJob, SIGNAL(data(KIO::Job*,QByteArray)), this, SLOT(transferedData(KIO::Job*,QByteArray)));
    connect(imageJob, SIGNAL(result(KJob*)), this, SLOT(transferFinished(KJob*)));
}
